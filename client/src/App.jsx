//importaciones de React y componentes principales.
import React from 'react';
import { Routes, Route } from 'react-router-dom';

//importaciones de componentes específicos.
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';

//importaciones de páginas.
import HomeNewsPage from './pages/HomeNewsPage/HomeNewsPage';
import NewsPage from './pages/NewsPage/NewPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import LoginPage from './pages/LoginPage/LoginPage';
import NotFoundPage from './pages/NotfoundPage/NotFoundPage';
import CreateNewsPage from './pages/CreateNewsPage/CreateNewsPage';
import EditNewsPage from './pages/EditNewsPage/EditNewsPage';

//Importaciones de Librerias.
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
    return (
        <div className='App'>
            <Header />
            <main>
                <Routes>
                    <Route path='/' element={<HomeNewsPage />} />
                    <Route path='/register' element={<RegisterPage />} />
                    <Route path='/login' element={<LoginPage />} />
                    <Route path='/createnews' element={<CreateNewsPage />} />
                    <Route path='/news/:id' element={<NewsPage />} />
                    <Route path='/news/:id/edit' element={<EditNewsPage />} />
                    <Route path='*' element={<NotFoundPage />} />
                </Routes>
            </main>
            <ToastContainer />
            <Footer />
        </div>
    );
};

export default App;
