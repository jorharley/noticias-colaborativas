// Importamos los hooks.
import { useContext } from 'react';

// Importamos el contexto.
import { AuthContext } from '../context/AuthContext';

export const useAuth = () => {
    return useContext(AuthContext);
};