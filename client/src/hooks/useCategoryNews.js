import { useState, useEffect } from 'react';
import getCategoryNewsService from '../services/getCategoryNewsService';

const useCategoryNews = (categoryId) => {
    const [categoryNews, setCategoryNews] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchCategoryNews = async () => {
            try {
                setLoading(true);

                const data = await getCategoryNewsService(categoryId);
                setCategoryNews(data.news);
            } catch (error) {
                setError(error.message);
            } finally {
                setLoading(false);
            }
        };

        fetchCategoryNews();
    }, [categoryId]);

    return { categoryNews, loading, error };
};

export default useCategoryNews;