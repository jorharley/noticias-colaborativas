//Importamos los estados.
import { useContext, useEffect, useState } from 'react';

//Importamos los services.
import getSingleNewService from '../services/getSingleNewService';
import { AuthContext } from '../context/AuthContext';

const useNew = (id) => {
    const [story, setStory] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const { token } = useContext(AuthContext);

    useEffect(() => {
        const loadNew = async () => {
            try {
                setLoading(true);

                const news = await getSingleNewService(id, token);

                setStory(news);
            } catch (error) {
                setError(error.message);
            } finally {
                setLoading(false);
            }
        };

        loadNew();
    }, [id, token]);

    return { story, setStory, loading, error };
};

export default useNew;
