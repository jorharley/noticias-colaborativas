//Importamos de hook de react.
import { useEffect, useState } from 'react';

//importamos los services.
import getAllNewsService from '../services/getAllNewsService';

const useNews = () => {
    const [news, setNews] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');

    useEffect(() => {
        //realizamos la petición para obtener las noticias (news)
        const loadNews = async () => {
            try {
                setLoading(true);

                const data = await getAllNewsService();

                setNews(data.news);
            } catch (error) {
                setError(error.message);
            } finally {
                setLoading(false);
            }
        };
        //Llamamos a la función anterior.
        loadNews();
    }, []);

    const addNews = (story) => {
        setNews([story, ...news]);
    };

    const removeNews = (id) => {
        setNews(news.filter((news) => news.id !== id));
    };

    return { news, loading, error, addNews, removeNews };
};

export default useNews;
