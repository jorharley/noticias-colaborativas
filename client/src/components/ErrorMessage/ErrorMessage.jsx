//Impotamos las prop.
import PropTypes from 'prop-types';

//Impotaciones de react
import { Link } from 'react-router-dom';

const ErrorMessage = ({ message }) => {
    return (
        <>
            <p> {message}</p>
            <Link to='/'>ir al inicio</Link>
        </>
    );
};

ErrorMessage.propTypes = {
    message: PropTypes.string,
};

export default ErrorMessage;
