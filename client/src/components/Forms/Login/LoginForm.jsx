//Importamos los componentes de react.
import { useNavigate } from 'react-router-dom';
//Importamos librerias de react
import { toast } from 'react-toastify';

//Importamos los estados.
import { useContext, useState } from 'react';

//importamos componentes

import Spinner from '../../Spinner/Spinner';

//Importamos el contexto.
import { AuthContext } from '../../../context/AuthContext';
import { loginUserService } from '../../../services/LoginUserServices';

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState('');
    const [errMessage, setErrMessage] = useState('');
    const { login } = useContext(AuthContext);
    const navigate = useNavigate();

    // función que maneja el envio del formulario.

    const handleSubmit = async (e) => {
        e.preventDefault();
        setErrMessage('');

        try {
            setLoading(true);

            const data = await loginUserService({ email, password });

            login(data);

            //si estamos logueados redireccionamos a el home
            navigate('/');

            //Enviamon una notificacion de usuario de bienvenida
            toast.success(`¡Bienvenido!`, {
                position: toast.POSITION.TOP_CENTER,
            });

            // Guardamos el token en el localStorage.
        } catch (err) {
            toast.error(err.message);
            setErrMessage(err.message);
        } finally {
            setLoading(false);
        }
    };

    return (
        <>
            <form
                className='newsForm'
                onSubmit={handleSubmit}
                aria-label='Formulario de registro'
            >
                <h2 className='titleNewsPages'>Login</h2>

                <label htmlFor='email'>Email:</label>
                <input
                    type='email'
                    id='email'
                    name='email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    autoComplete='email'
                    required
                    aria-label='Dirección de correo electrónico'
                />

                <label htmlFor='password'>Contraseña:</label>
                <input
                    type='password'
                    id='password'
                    value={password}
                    name='password'
                    onChange={(e) => setPassword(e.target.value)}
                    minLength='8'
                    maxLength='100'
                    autoComplete='password'
                    required
                    aria-label='Contraseña'
                />
                <button
                    className='buttonNews'
                    disabled={loading}
                    aria-label='login'
                >
                    Inicia sesión
                </button>

                {loading && <Spinner />}

                {errMessage ? <p className='error'>{errMessage}</p> : null}
            </form>
        </>
    );
};

export default LoginForm;
