//importamos los estados.
import { useState } from 'react';

//importamos los hook
import { useNavigate } from 'react-router-dom';

//importamos los prop-types para especificar los props.

//importamos componentes que manejan la comunicacion con el backend
import Spinner from '../../Spinner/Spinner';

//importamos css.
import '../../Shared/FormStyle/formStyle.css';

//Importamos librerías de react.
import { toast } from 'react-toastify';
import { registerUserService } from '../../../services/registerUserSErvice';

const RegisterForm = () => {
    const navigate = useNavigate();
    const [lastName, setlastName] = useState('');
    const [firstName, setfirstName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [bio, setBio] = useState('');
    const [avatar, setAvatar] = useState('');
    const [previewPhoto, setPreviewPhoto] = useState('');
    const [loading, setLoading] = useState('');
    const [errMessage, setErrMessage] = useState('false');

    // función que maneja el envio del formulario.

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            setLoading(true);

            await registerUserService({
                lastName,
                firstName,
                email,
                password,
                bio,
                avatar,
            });
            //Redireccionamos a login.
            navigate('/login');

            //Enviamos una notificacion de la alerta que el registro se ha creado
            toast.success('Usuario registrado exitosamente');
        } catch (err) {
            toast.error(err.message);
            setErrMessage(err.message);
        } finally {
            setLoading(false);
        }
    };

    return (
        
            <form
                className='newsForm'
                onSubmit={handleSubmit}
                aria-label='Formulario de registro'
            >
                <h2 className='titleNewsPages'>Registro</h2>

                <label htmlFor='lastName'>Nombre:</label>
                <input
                    type='text'
                    id='lastName'
                    value={lastName}
                    onChange={(e) => setlastName(e.target.value)}
                    autoComplete='lastName'
                    autoFocus
                    required
                    aria-label='Nombre de usuario'
                />

                <label htmlFor='firstName'>Apellido:</label>
                <input
                    type='text'
                    id='firstName'
                    value={firstName}
                    onChange={(e) => setfirstName(e.target.value)}
                    autoComplete='firstName'
                    autoFocus
                    required
                    aria-label='Apellido de usuario'
                />

                <label htmlFor='email'>Email:</label>
                <input
                    type='email'
                    id='email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    autoComplete='email'
                    required
                    aria-label='Dirección de correo electrónico'
                />

                <label htmlFor='password'>Contraseña:</label>
                <input
                    type='password'
                    id='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    minLength='08'
                    maxLength='100'
                    autoComplete='password'
                    required
                    aria-label='Contraseña'
                />

                <label htmlFor='pass'>Bio:</label>
                <textarea
                    placeholder='Biografía'
                    value={bio}
                    onChange={(e) => setBio(e.target.value)}
                />

                <label htmlFor='avatar' className='image'>Seleccionar foto:</label>
                <input
                    type='file'
                    id='avatar'
                    accept='image/*'
                    onChange={(e) => setPreviewPhoto(e.target.files[0])}
                    aria-label='Avatar'
                />
                {previewPhoto ? (
                    <img
                        className='avatarPreview'
                        src={URL.createObjectURL(previewPhoto)}
                        alt='foto previa'
                        style={{ width: '100px' }}
                    />
                ) : null}

                {loading ? (
                    <Spinner aria-label='Cargando' />
                ) : (
                    <button className='buttonNews'
                        type='submit'
                        disabled={loading}
                        aria-label='Registrarse'
                    >
                        Registrarse
                    </button>
                )}
            </form>
        
    );
};

export default RegisterForm;
