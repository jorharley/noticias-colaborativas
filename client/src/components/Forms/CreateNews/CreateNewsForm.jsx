//Importamos componetes de react
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

//Importamos librerias de rect.
import { toast } from 'react-toastify';

//Importamos Services
import newsCreateService from '../../../services/newsCreateService';
import getCategories from '../../../services/getCategories';

//Importamos Contexto
import { AuthContext } from '../../../context/AuthContext';

//Importamos el css
import '../../Shared/FormStyle/formStyle.css';

const CreateNewsForm = () => {
    const [err, setErr] = useState('');
    const [previewPhoto, setPreviewPhoto] = useState('');
    const [seading, setseading] = useState('false');
    const [categories, setCategories] = useState([]);
    const { token } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        // Cargar las categorías al montar el componente
        const fetchCategories = async () => {
            try {
                const categoriesData = await getCategories();
                setCategories(categoriesData);
            } catch (err) {
                toast.error(err.message);
                setErr(err.message);
            }
        };
        fetchCategories();
    }, []);

    const handleForm = async (e) => {
        e.preventDefault();

        try {
            setseading(true);

            const data = new FormData(e.target);
            const insertId = await newsCreateService({ data, token });

            // Obtener la categoría seleccionada
            const selectedCategory = data.get('categoryId');
            const selectedCategoryName = categories.find(
                (category) => category.id === parseInt(selectedCategory)
            )?.name;

            //Redireccionamos a la noticia creada.
            navigate(`/`);

            //Enviamos una notificacion de noticia creada.
            toast.success('Noticia Creada', {
                position: toast.POSITION.TOP_CENTER,
            });
        } catch (err) {
            toast.error(err.message);
            setErr(err.message);
        } finally {
            setseading(false);
        }
    };

    return (
        <>
            <form className='newsForm' onSubmit={handleForm}>
                <h2 className='titleNewsPages'>Crear Nueva Noticia</h2>

                <label className='titleNews' htmlFor='title'>
                    Título de la noticia:
                </label>
                <input type='text' id='title' name='title' required />

                <label htmlFor='leadIn' className='leadInNews'>
                    Lead-in:
                </label>
                <input type='text' id='leadIn' name='leadIn' required />

                <label htmlFor='body' className='bodyNews'>
                    Cuerpo de la noticia:
                </label>
                <textarea id='body' name='body' required />

                <label className='categoryNews' htmlFor='category'>
                    Categoría:
                </label>
                <select
                    className='categoryCreateNews'
                    id='category'
                    name='categoryId'
                    required
                >
                    <option value=''>-- Seleccionar categoría --</option>
                    {categories.map((category) => (
                        <option key={category.id} value={category.id}>
                            {category.name}
                        </option>
                    ))}
                </select>

                <label htmlFor='photo' className='image'>
                    Seleccionar foto:
                </label>
                <input
                    type='file'
                    id='photo'
                    accept='image/*'
                    name='photo'
                    onChange={(e) => setPreviewPhoto(e.target.files[0])}
                />

                {previewPhoto ? (
                    <img
                        className='image'
                        src={URL.createObjectURL(previewPhoto)}
                        alt='foto previa'
                        style={{ width: '100px' }}
                    />
                ) : null}

                <button className='buttonNews' type='submit'>
                    Crear Noticia
                </button>
            </form>
        </>
    );
};

export default CreateNewsForm;
