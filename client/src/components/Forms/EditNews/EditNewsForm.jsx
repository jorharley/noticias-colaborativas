//importamos componetes de react.
import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

//Importamos las librerias.
import { toast } from 'react-toastify';

//Importamos Contexto
import { AuthContext } from '../../../context/AuthContext';

//importamos los services.
import updateNewsService from '../../../services/updateNewsService';
import getSingleNewService from '../../../services/getSingleNewService';

//Importamos css
import '../../Shared/FormStyle/formStyle.css';

const EditNewsForm = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const { token } = useContext(AuthContext);

    const [title, setTitle] = useState('');
    const [leadIn, setLeadIn] = useState('');
    const [body, setBody] = useState('');
    const [photo, setPhoto] = useState(null);

    useEffect(() => {
        // Cargar los datos de la noticia existente al montar el componente
        const fetchNews = async () => {
            try {
                const news = await getSingleNewService(id, token);

                setTitle(news.title);
                setLeadIn(news.leadIn);
                setBody(news.body);
            } catch (error) {
                console.error(error);
            }
        };
        fetchNews();
    }, [id, token]);

    const handleUpdateNews = async (e) => {
        e.preventDefault();

        try {
            // Llamar al servicio para actualizar la noticia
            await updateNewsService(id, { title, leadIn, body, photo }, token);

            toast.success('Noticia actualizada exitosamente', {
                position: toast.POSITION.TOP_CENTER,
            });

            // Redireccionar a la página de detalles de la noticia
            navigate(`/news/${id}`);
        } catch (error) {
            console.error(error);
            toast.error('Error al actualizar la noticia', {
                position: toast.POSITION.TOP_CENTER,
            });
        }
    };

    return (
        <div className='editNewsForm'>
            <form className='newsForm' onSubmit={handleUpdateNews}>
                <h2 className='titleNewsPages'>Editar Noticia</h2>

                <label className='titleNews' htmlFor='title'>
                    Título:
                </label>
                <input
                    type='text'
                    id='title'
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    required
                />

                <label className='leadInNews' htmlFor='leadIn'>
                    Lead In:
                </label>
                <textarea
                    id='leadIn'
                    value={leadIn}
                    onChange={(e) => setLeadIn(e.target.value)}
                    required
                />

                <label htmlFor='body'>Cuerpo:</label>
                <textarea
                    id='body'
                    value={body}
                    onChange={(e) => setBody(e.target.value)}
                    required
                />

                <label className='photo' htmlFor='photo'>
                    Imagen:
                </label>
                <input
                    type='file'
                    id='photo'
                    accept='image/*'
                    onChange={(e) => setPhoto(e.target.files[0])}
                />

                <button type='submit' className='buttonNews'>Actualizar Noticia</button>
            </form>
        </div>
    );
};

export default EditNewsForm;
