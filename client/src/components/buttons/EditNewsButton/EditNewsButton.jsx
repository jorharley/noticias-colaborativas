//Importaciones de react.
import { Link } from 'react-router-dom';

//Importaciones las props.
import PropTypes from 'prop-types';

const EditNewsButton = ({ newsId }) => {
  return (
    <Link to={`/news/${newsId}/edit`} className="Link">
      Editar
    </Link>
  );
};

EditNewsButton.propTypes = {
  newsId: PropTypes.number.isRequired,
};

export default EditNewsButton;