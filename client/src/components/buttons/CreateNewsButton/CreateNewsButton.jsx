//importaciones de react-router
import { Link } from 'react-router-dom';

//Impotaciones css.
import './createNewsButton.css'

const CreateNewsButton = () => {
    return (
        <section>
            <Link
                to='/createnews'
                className='buttonCustom'
            >
                Pública tu Noticia
            </Link>
        </section>
    );
};

export default CreateNewsButton;
