//Importamos los iconos.
import ThumbsUp from '../ThumbsUp/ThumbsUp';
import ThumbsDown from '../ThumbsDown/ThumbsDown';

//Importamos el ccs.
import './VoteButton.css';

const VoteButton = ({
    positiveVotes,
    negativeVotes,
    likedByMe,
    dislikedByMe,
    onVote,
}) => {
    return (
        <div className='voteButton'>
            <button
                onClick={() => onVote(1)}
                className={`voteButton ${likedByMe === 1 ? 'positive' : ''}`}
            >
                <ThumbsUp /> {positiveVotes}
            </button>
            <button
                onClick={() => onVote(0)}
                className={`voteButton ${dislikedByMe === 1 ? 'negative' : ''}`}
            >
                <ThumbsDown />
                {negativeVotes}
            </button>
        </div>
    );
};

export default VoteButton;
