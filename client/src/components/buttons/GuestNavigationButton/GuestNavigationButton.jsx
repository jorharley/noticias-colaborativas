//importaciones de react-router
import { Link } from 'react-router-dom';

//Importaciones de contexto.
import { AuthContext } from '../../../context/AuthContext';
import { useContext } from 'react';

//importaciones de css.
import './GuestNavigationButton.css';

const GuestNavigationButton = () => {
    const { user, logout } = useContext(AuthContext);

    return user ? (     
        <div className='navUser'>
            <p className='nameWithLogout'>Hola, {user.lastName} {user.firstName}</p>
            <button className='winthLogout' onClick={() => logout()}>Logout</button>   
        </div>   
        ) : (
        <ul className='Links'>
            <li className='register'>
                <Link to='/register' className='Link'>
                    Register
                </Link>
            </li>
            <li className='login'>
                <Link to='/Login' className='Link'>
                    Login
                </Link>
            </li>

        </ul>
    );
};

export default GuestNavigationButton;
