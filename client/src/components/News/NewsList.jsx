//import de react
import { Link } from 'react-router-dom';
import { formatDistanceToNow } from 'date-fns';

//import CSS
import './News.css';
import ThumbsDown from '../buttons/ThumbsDown/ThumbsDown';
import ThumbsUp from '../buttons/ThumbsUp/ThumbsUp';

const NewsList = ({ news, removeNews }) => {
    return news && news.length ? (
        <ul className='listaNews'>
            {news.map((news) => (
                <li className='liNews' key={news.id}>
                    <div className='newsContent'>
                        <p className='category' to={news.categoryId}>
                            {news.categoryName}
                        </p>
                        <Link to={`/news/${news.id}`} className='linkTitle'>
                            <p className='title'>{news.title}</p>
                        </Link>

                        <p className='createAd'>
                            Hace{' '}
                            {formatDistanceToNow(new Date(news.createdAt), {
                                addSuffix: true,
                            })}
                        </p>

                        <p className='author'>
                            Por {news.lastName} {''} {news.firstName} {''}
                        </p>

                        <p className='leadId'>{news.leadIn}</p>

                        <div className='votes'>
                            <p className='positiveVotes'>
                                <ThumbsUp />
                                {news.positiveVotes}
                            </p>
                            <p className='negativeVote'>
                                <ThumbsDown /> {news.negativeVotes}
                            </p>
                        </div>
                    </div>

                    {news.photo && (
                        <img
                            className='newsImage'
                            src={`http://localhost:8000/${news.photo}`}
                            alt='Imagen adjunta a la noticia.'
                        />
                    )}
                </li>
            ))}
        </ul>
    ) : (
        <p>no hay noticias</p>
    );
};

export default NewsList;
