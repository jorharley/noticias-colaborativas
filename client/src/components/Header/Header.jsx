//Importaciones de react.
import { NavLink } from 'react-router-dom';

//Importamos los componentes.
import GuestNavigationButton from '../buttons/GuestNavigationButton/GuestNavigationButton';

//Importamos el ccs.
import './Header.css';


const Header = () => {
    
    return (
        <header className='header'>
            <h1>
                    {' '}
                <NavLink to='/' className='LinkLogo'>
                    HackaNews{' '}
                </NavLink>
            </h1>
            <nav className='botonLogin'>
                 <GuestNavigationButton/>
            </nav>
            
        </header>
    );
};

export default Header;
