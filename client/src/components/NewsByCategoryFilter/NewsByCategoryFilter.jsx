import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const NewsByCategoryFilter = ({ categories }) => {
    const [selectedCategoryId, setSelectedCategoryId] = useState('');

    const navigate = useNavigate();

    const handleCategoryChange = (e) => {
        const categoryId = e.target.value;
        setSelectedCategoryId(categoryId);

        // Redireccionar a la página de noticias filtradas por categoría
        if (categoryId) {
            navigate(`/news?category=${categoryId}`);
        } else {
            navigate('/news');
        }
    };

    return (
        <div className='categoryFilter'>
            <select value={selectedCategoryId} onChange={handleCategoryChange}>
                <option value=''>Todas las categorías</option>
                {categories.map((category) => (
                    <option key={category.id} value={category.id}>
                        {category.name}
                    </option>
                ))}
            </select>
        </div>
    );
};

export default NewsByCategoryFilter;
