//Importamos el ccs.
import './Footer.css';

const Footer = () => {
    return <footer className="footer">
        <p>Copyright &copy; hackaNews 2023</p>
    </footer>
}

export default Footer;