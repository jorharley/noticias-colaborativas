const getCategoriesService = async () => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/categories`);
    const body = await res.json();
  
    if (!res.ok) {
      throw new Error(body.message);
    }
  
    return body.data.categories;
  };
  
  export default getCategoriesService;