const newsCreateService = async ({ data, token }) => {
    const response = await fetch(`${process.env.REACT_APP_BACKEND}/news`, {
        method: 'POST',
        headers: {
            Authorization: token,
        },
        body: data,
    });

    const body = await response.json();
    if (!response.ok) {
        throw new Error(body.message);
    }

    return body.insertId;
};

export default newsCreateService;
