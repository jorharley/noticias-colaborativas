const getAllNewsService = async () => {
    const res= await fetch(`${process.env.REACT_APP_BACKEND}/news`);

    const body = await res.json();
    if (!res.ok) {
        throw new Error(body.message);
    }
    return body.data;
};

export default getAllNewsService;