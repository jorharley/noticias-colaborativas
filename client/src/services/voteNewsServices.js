const voteNewsService = async ({ newsId, likes, token }) => {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND}/news/${newsId}/votes`,
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                likes, // 'positive' o 'negative'
            }),
        }
    );

    const body = await response.json();

    if (!response.ok) {
        throw new Error(body.message);
    }

    return body;
};

export default voteNewsService;
