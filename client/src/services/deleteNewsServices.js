const deleteNewsServices = async ({ id, token }) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/news/${id}`, {
        method: 'DELETE',
        headers: {
            Authorization: token,
        },
    });

    const body = await res.json();
    if (!res.ok) {
        throw new Error(body.message);
    }

    return body.data;
};

export default deleteNewsServices;
