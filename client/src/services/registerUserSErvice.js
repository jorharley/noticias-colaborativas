export const registerUserService = async ({
    lastName,
    firstName,
    email,
    password,
    bio,
    avatar,
}) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/users`, {
        method: 'post',
        headers: {
            
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            password,
            lastName,
            firstName,
            bio,
            avatar,
        }),
    });

    const body = await res.json();

    if (!res.ok) {
        throw new Error(body.message);
    }

    return body.message;
};


