const updateNewsService = async (id, updatedData, token) => {
    const formData = new FormData();

    formData.append('title', updatedData.title);
    formData.append('leadIn', updatedData.leadIn);
    formData.append('body', updatedData.body);

    if (updatedData.photo) formData.append('photo', updatedData.photo);

    const res = await fetch(`${process.env.REACT_APP_BACKEND}/news/${id}`, {
        method: 'PUT',
        headers: {
            Authorization: token,
        },
        body: formData,
    });

    const body = await res.json();

    if (!res.ok) {
        throw new Error(body.message);
    }

    return body.message;
};

export default updateNewsService;
