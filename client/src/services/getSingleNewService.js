const getSingleNewService = async (id, token) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/news/${id}`, {
        headers: {
            Authorization: token,
        },
    });

    const body = await res.json();

    if (!res.ok) {
        throw new Error(body.message);
    }

    return body.data.news;
};

export default getSingleNewService;
