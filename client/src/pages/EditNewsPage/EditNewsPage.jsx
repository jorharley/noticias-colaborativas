//Importamos Componentes.
import EditNewsForm from '../../components/Forms/EditNews/EditNewsForm';

const EditNewsPage = () => {
    // Obtén los datos de la noticia que deseas editar
    const newsData = {
        id: 1, // ID de la noticia
        title: 'Título de la noticia',
        leadIn: 'Lead-in de la noticia',
        body: 'Cuerpo de la noticia',
    };

    return (
        <div>
            <EditNewsForm newsData={newsData} />
        </div>
    );
};

export default EditNewsPage;
