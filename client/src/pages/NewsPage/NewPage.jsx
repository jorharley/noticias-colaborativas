// Importamos los componentes de react.
import { useContext, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

//Importamos los hooks.
import useNew from '../../hooks/useNew';

//Importamos el contexto.
import { AuthContext } from '../../context/AuthContext';

//Importamos librerias.
import { toast } from 'react-toastify';
import { formatDistanceToNow } from 'date-fns';

// Importamos los componentes personalizados.
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import VoteButton from '../../components/buttons/VotesButton/VoteButton';
import EditNewsButton from '../../components/buttons/EditNewsButton/EditNewsButton';

//Importamos los servicios.
import deleteNewsServices from '../../services/deleteNewsServices';
import voteNewsService from '../../services/voteNewsServices';

// Importamos el CSS.
import './NewPage.css';

const NewsPage = (removeNews) => {
    const { id } = useParams();

    const { story, setStory, loading, error } = useNew(id);

    const { user, token } = useContext(AuthContext);
    const [errMessage, setErrMessage] = useState('');
    const navigate = useNavigate();

    const deleteNews = async () => {
        try {
            // Mostrar confirmación al usuario.
            const confirmDelete = window.confirm(
                '¿Estás seguro de que quieres eliminar esta noticia?'
            );

            // Si el usuario confirma la eliminación.
            if (confirmDelete) {
                await deleteNewsServices({ id, token });

                //Redireccionamos a la pagina de inicio.
                navigate('/');

                //Enviamos un mensaje de Noticia eliminada.
                toast.success('noticia eliminada', {
                    position: toast.POSITION.TOP_CENTER,
                });
            }
        } catch (error) {
            // Mostrar mensaje de error en caso de fallo
            toast.error(error.Message, {
                position: toast.POSITION.TOP_CENTER,
            });
        }
    };

    if (loading) return <p>cargando new</p>;
    if (error) return <ErrorMessage message={error} />;

    const handleVote = async (voteValue) => {
        try {
            if (!user) {
                // Manejo si el usuario no está autenticado
                toast.error('Debes iniciar sesión para votar.', {
                    position: toast.POSITION.TOP_CENTER,
                });
                return;
            }

            const data = await voteNewsService({
                newsId: story.id,
                likes: voteValue,
                token,
            });

            setStory((prevStory) => ({
                ...prevStory,
                ...data,
            }));

            toast.success('Voto registrado exitosamente.', {
                position: toast.POSITION.TOP_CENTER,
            });
            window.location.reload();
        } catch (error) {
            toast.error('Error al registrar el voto.', {
                position: toast.POSITION.TOP_CENTER,
            });
        }
    };

    return (
        <div className='newsContainer'>
            {story && (
                <>
                    <header className=' headerNewsPages'>
                        <p className='category' to={story.categoryId}>
                            {story.categoryName}
                        </p>
                        <h2 className='titleNewsPage'> {story.title}</h2>
                        <p className='author'>
                            Por {story.lastName} {story.firstName}
                        </p>
                        <p className='createAd'>
                            Hace{' '}
                            {formatDistanceToNow(new Date(story.createdAt), {
                                addSuffix: true,
                            })}
                        </p>
                    </header>

                    <section className='newsMain'>
                        <div className='votesNews'>
                            <VoteButton
                                positiveVotes={story.positiveVotes}
                                negativeVotes={story.negativeVotes}
                                likedByMe={story.likedByMe}
                                dislikedByMe={story.dislikedByMe}
                                onVote={handleVote}
                            />
                        </div>
                        <p className='leadIn'>{story.leadIn}</p>
                        {story.photo && (
                            <img
                                className='photo'
                                src={`${process.env.REACT_APP_BACKEND}/${story.photo}`}
                                alt='foto de la noticia'
                            />
                        )}
                        <p className='body'>{story.body}</p>
                    </section>
                </>
            )}
            {user?.id === story?.userId && (
                <>
                    <section className='buttonUser'>
                        <button
                            className='buttonDelete'
                            onClick={() => deleteNews(story.id)}
                        >
                            Eliminar
                        </button>
                    </section>
                    <section>
                        <EditNewsButton newsId={story.id} />
                    </section>
                </>
            )}
        </div>
    );
};

export default NewsPage;
