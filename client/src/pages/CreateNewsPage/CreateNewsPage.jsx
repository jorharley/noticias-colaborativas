//Importamos los componentes de react.
import { Navigate } from 'react-router-dom';

//Importamos los componentes personalizados.
import CreateNewsForm from '../../components/Forms/CreateNews/CreateNewsForm';

//Importamos el contexto.
import { useAuth } from '../../hooks/useAuth';

const NewCreatePage = () => {
    const { token } = useAuth();

    // Si la persona NO está logeada la redirigimos a la página principal.
    if (!token) return <Navigate to='/' />;

    return (
        <main className='newCreate'>
            <CreateNewsForm />
        </main>
    );
};

export default NewCreatePage;