// Importamos los hooks.
import { useAuth } from '../../hooks/useAuth';

// Importamos los componentes de react.
import { Navigate } from 'react-router-dom';

//Importamos los componentes personalizados.
import LoginForm from '../../components/Forms/Login/LoginForm';


const LoginPage = () => {
    const { token, login } = useAuth();

    // Si la persona está logeada la redirigimos a la página principal.
    if (token) return <Navigate to='/' />;

    return (
        <main>
            <LoginForm login={login} />
        </main>
    );
};

export default LoginPage;
