//Importamos el contexto.
import { useContext } from 'react';
import { AuthContext } from '../../context/AuthContext';

//Importamos los componentes personalizados.
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import NewsList from '../../components/News/NewsList';
import useNews from '../../hooks/useNews';
import CreateNewsButton from '../../components/buttons/CreateNewsButton/CreateNewsButton';

const HomeNewsPage = () => {
    //devuelve las noticias.
    const { news, loading, error, addNews, removeNews } = useNews();
    const { user } = useContext(AuthContext);

    if (loading) return <p>cargando noticias </p>;

    return (
        <>
            <>
                <section>{user ? <CreateNewsButton /> : null}</section>

                <section className='homePages'>
                    <NewsList news={news} removeNews={removeNews} />
                </section>
            </>
        </>
    );
};

export default HomeNewsPage;
