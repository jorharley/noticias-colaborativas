// Importamos los hooks.
import { useAuth } from '../../hooks/useAuth';

// Importamos los componentesde react.
import { Navigate } from 'react-router-dom';

//Importamos los componentes personalizados.
import RegisterForm from '../../components/Forms/Register/RegisterForms';

const RegisterPage = () => {
    const { token } = useAuth();

    // Si la persona está logeada la redirigimos a la página principal.
    if (token) return <Navigate to='/' />;

    return (
        <main className='main'>
            <RegisterForm />
        </main>
    );
};

export default RegisterPage;
