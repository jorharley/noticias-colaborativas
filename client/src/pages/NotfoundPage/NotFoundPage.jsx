//importaciones de componentes de react.
import { Link } from 'react-router-dom';

const NotFoundPage = () => {
    return (
        <>
            <h2>Not Found</h2>
            <Link to={'/'}>Ir a la página de inicio</Link>
        </>
    );
};

export default NotFoundPage;
