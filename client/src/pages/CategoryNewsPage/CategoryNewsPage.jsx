import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import useNews from '../../Hook/useNews'; // El hook para obtener noticias
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import NewsList from '../../components/News/NewsList';

const CategoryNewsPage = () => {
    const { id } = useParams();
    const { news, loading, error } = useNews(); // Usar el hook para obtener las noticias
    const [categoryNews, setCategoryNews] = useState([]);

    useEffect(() => {
        // Filtrar las noticias por la categoría seleccionada
        const filteredNews = news.filter((item) => item.categoryId === id);
        setCategoryNews(filteredNews);
    }, [news, id]);

    if (loading) return <p>Cargando noticias...</p>;
    if (error) return <ErrorMessage message={error} />;

    return (
        <div className='categoryNewsPage'>
            <h2>Noticias por Categoría</h2>
            {categoryNews.length === 0 ? (
                <p>No hay noticias en esta categoría</p>
            ) : (
                <NewsList news={categoryNews} />
            )}
        </div>
    );
};

export default CategoryNewsPage;
