//Importamos los estados.
import { createContext, useEffect, useState } from 'react';

//Importamos las prop.
import PropTypes from 'prop-types';

//Importamos los servicios.
import getMyDataService from '../services/getMyDataService';

//Importamos el contexto.
const AuthContext = createContext(null);

const AuthProviderComponent = ({ children }) => {
    const [token, setToken] = useState(localStorage.getItem('token'));
    const [user, setUser] = useState(null);

    useEffect(() => {
        localStorage.setItem('token', token);
    }, [token]);

    //useEffect para ejecutar cuando cambia el token.

    useEffect(() => {
        const getUserData = async () => {
            try {
                const data = await getMyDataService({ token });
                
                setUser(data);
            } catch (err) {
                console.log (err.message);
                logout();
            }
        };
        //Si el usuario existe obtenemos los datos del usuario.
        if (token) getUserData();
    }, [token]);

    //funcion que recibe un token.

    const login = (newtoken) => {
        localStorage.setItem('token', newtoken);
        setToken(newtoken);
    };

    //funcion que coloque el token vacio y null.
    const logout = () => {
        localStorage.removeItem('token');
        setToken('');
        setUser(null);
    };

    return (
        <AuthContext.Provider value={{ token, user, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
};

AuthProviderComponent.propTypes = {
    children: PropTypes.node,
};

export { AuthContext, AuthProviderComponent };
