# Proyecto Final. Noticias Colaborativas.

## descripción.
Implementar el frontend usando React del proyecto anterior que se pedia crear una API que permita gestionar noticias colaborativas, estilo reddit o menéame,
donde los usuarios puedan registrarse y publicar una noticia en una serie de categorías temáticas fijas.

### Estructura del proyecto.
frontend/
├── node_modules/ # Dependencias del frontend (generado automáticamente)
├── public/ # Archivos públicos del frontend (HTML, imágenes, etc.)
├── src/ # Código fuente del frontend
├── .gitignore # Archivo de exclusión para Git
├── package.json # Archivo de configuración del frontend
└── README.md # Documentación específica del frontend (este archivo)

#### Configuración y Uso

1. Ingresa a la carpeta `frontend`:
   cd frontend

2. Instala las dependencias:
    npm install

3.Inicia la aplicación:
    npm start

##### Caracterísicas.

- Interfaz de usuario intuitiva.
- Navegación fluida entre páginas.
- Integración con API de backend.
- Responsive.