const joi = require('joi');

const editUserSchema = joi.object().keys({
  firstName: joi
    .string()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere un nombre');
      } else {
        return new Error('El nombre no es válido');
      }
    }),

  lastName: joi
    .string()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere mínimo un apellido');
      } else {
        return new Error('El apellido no es válido');
      }
    }),

  email: joi
    .string()
    .email()
    .required()
    .error((errors) => {
      if (errors[0].code === 'any.required') {
        return new Error('Se requiere un email');
      } else {
        return new Error('El email no es válido');
      }
    }),

  bio: joi.string().optional(),
  avatar: joi.object().optional(),
});

module.exports = editUserSchema;
