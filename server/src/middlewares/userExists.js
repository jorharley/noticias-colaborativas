const getDB = require('../db/getDB');

const generateError = require('../services/generateError');

const userExists = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Comprobamos si existe un usuario con el id que obtenemos del token.
    const [users] = await connection.query(
      `SELECT id FROM users WHERE id = ?`,
      [req.user.id]
    );

    // Si el array de usuarios tiene una longitud menor que 1 lanzamos un error.
    if (users.length < 1 && req.user.id !== 0) {
      generateError('Usuario no encontrado', 404);
    }

    // Saltamos a la siguiente función controladora.
    next();
  } catch (err) {
    next(err);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = userExists;
