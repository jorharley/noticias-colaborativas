const express = require('express');
const router = express.Router();

// Importamos los middlewares personalizados.
const authUser = require('../middlewares/authUser');
const authUserOptional = require('../middlewares/authUserOptional');
const userExists = require('../middlewares/userExists');

const {
  createNews,
  listNews,
  getNews,
  deleteNews,
  updateNews,
  newVote,
  getCategories,
} = require('../controllers/news/index');

// Registro de un nuevo usuario.
router.post('/news', authUser, userExists, createNews);

// Listado de noticias
router.get('/news', authUserOptional, userExists, listNews);

// Recuperar una noticia en concreto
router.get('/news/:newsId', authUserOptional, userExists, getNews);

// Eliminar una noticia (solo si se es propietario)
router.delete('/news/:newsId', authUser, userExists, deleteNews);

// Actualizar/modificar una noticia
router.put('/news/:newsId', authUser, userExists, updateNews);

// Efectua un voto
router.post('/news/:newsId/votes', authUser, userExists, newVote);

// Lista de noticias
router.get('/categories', authUserOptional, userExists, getCategories);

module.exports = router;
