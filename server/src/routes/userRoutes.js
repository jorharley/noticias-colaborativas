const express = require('express');
const router = express.Router();

// Importamos los middlewares personalizados.
const authUser = require('../middlewares/authUser');
const authUserOptional = require('../middlewares/authUserOptional');
const userExists = require('../middlewares/userExists');

const {
  newUser,
  loginUser,
  editUser,
  getUser,
  getUserPublic,
} = require('../controllers/users/index');

// Registro de un nuevo usuario.
router.post('/users', newUser);

// Login de usuario.
router.post('/users/login', loginUser);

// Actualizar datos del usuario.
router.put('/users', authUser, userExists, editUser);

// Recupera información privada de un usuario
router.get('/users', authUser, userExists, getUser);

// Recupera información pública de un usuario
router.get('/users/:userId', authUserOptional, userExists, getUserPublic);

module.exports = router;
