const getNewsQuery = require('../../models/news/getNewsQuery');

const getNews = async (req, res, next) => {
  try {
    const { newsId } = req.params;

    const news = await getNewsQuery(newsId, req.user?.id);

    res.send({
      status: 'ok',
      data: {
        news,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = getNews;
