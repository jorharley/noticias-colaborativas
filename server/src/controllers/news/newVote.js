const getNewsQuery = require('../../models/news/getNewsQuery');
const newVoteQuery = require('../../models/news/newVoteQuery');

const generateError = require('../../services/generateError');

const newVote = async (req, res, next) => {
    try {
        const { newsId } = req.params;

        const { likes } = req.body;

        // Nos aseguramos que los parametros del voto sean correctos.
        if (likes !== 0 && likes !== 1) {
            generateError(
                'Voto invalido. Vota 1 para positivo o 0 para negativo',
                400
            );
        }

        // Comprobamos que existe la noticia
        await getNewsQuery(newsId, req.user.id);

        // Comprobamos si el usuario ya ha votado y actualizamos la DB con los nuevos datos.
        await newVoteQuery(newsId, Number(likes), req.user.id);

        res.send({
            status: 'ok',
            message: 'Voto registrado!',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newVote;
