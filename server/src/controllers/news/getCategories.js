const getCategoriesQuery = require('../../models/news/getCategoriesQuery');

const getCategories = async (req, res, next) => {
  try {
    const categories = await getCategoriesQuery();

    res.send({
      status: 'ok',
      data: {
        categories,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = getCategories;
