const newUser = require('./newUser');
const loginUser = require('./loginUser');
const editUser = require('./editUser');
const getUser = require('./getUser');
const getUserPublic = require('./getUserPublic');

module.exports = {
  newUser,
  loginUser,
  editUser,
  getUser,
  getUserPublic,
};
