const getDB = require('../../db/getDB');

const getCategoriesQuery = async () => {
  let connection;

  try {
    connection = await getDB();

    const [categories] = await connection.query(
      `
      SELECT id, name FROM categories
    `
    );

    return categories;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getCategoriesQuery;
