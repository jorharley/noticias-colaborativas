const getDB = require('../../db/getDB');

const newVoteQuery = async (newsId, likes, userId) => {
  let connection;

  try {
    connection = await getDB();

    // Verificamos si el usuario ya ha votado la publicacion.

    const [votes] = await connection.query(
      `
        SELECT id, likes, newsId, userId, createdAt
        FROM votes WHERE newsId = ? AND userId = ?
      `,
      [newsId, userId]
    );

    if (votes.length > 0) {
      // Si el usuario ya ha emitido un voto, verificamos si la opcion que elige es diferente a la anterior.

      if (likes !== votes[0].likes) {
        // Actualizamos el voto existente con la nueva decision.
        await connection.query(`UPDATE votes SET likes = ? WHERE id = ?`, [
          likes,
          votes[0].id,
        ]);

        // Si el usuario elige la misma opcion, anulamos el voto.
      } else {
        await connection.query(`DELETE FROM votes WHERE id = ?`, [votes[0].id]);
      }

      // Si el usuario no ha votado anadimos el nuevo voto.
    } else {
      await connection.query(
        `INSERT INTO votes (likes, newsId, userId, createdAt) 
         VALUES (?, ?, ?, ?)`,
        [likes, newsId, userId, new Date()]
      );
    }
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newVoteQuery;
