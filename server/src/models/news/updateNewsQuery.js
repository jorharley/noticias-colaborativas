const getDB = require('../../db/getDB');

const updateNewsQuery = async (
  title,
  leadIn,
  body,
  categoryId,
  photo,
  newsId
) => {
  let connection;

  try {
    connection = await getDB();

    await connection.query(
      `UPDATE news SET title = ?, leadIn = ?, body = ?,
       categoryId = ?, photo = ?, modifiedAt = ? WHERE id = ?`,
      [title, leadIn, body, categoryId, photo, new Date(), newsId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateNewsQuery;
